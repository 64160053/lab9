package com.preeya.robot;

public class Map {
    private int width;
    private int height;
    private Unit units[];
    private int unitCount;

    public Map(int widhth, int height) {
        this.width = widhth;
        this.height = height;
        this.unitCount = 0;
        this.units = new Unit[widhth * height];
    }

    public Map() {
        this(10, 10);
    }

    public void print() {
        for (int y = 0; y < this.height; y++) {
            for (int x = 0; x < this.height; x++) {
                printBox(x, y);
            }
            System.out.println();
        }
    }

    private void printBox(int x, int y) {
        for (int i = 0; i < unitCount; i++) {
            Unit unit = this.units[i];
            if (unit.isOn(x, y)) {
                System.out.print(unit.getSymbol());
                return;
            }
        }
        System.out.print("-");
    }

    public String toString() {
        return "Map(" + this.width + "," + this.height + ")";
    }

    public void add(Unit unit) {
        if (unitCount == (width * height))
            return;
        this.units[unitCount] = unit;
        unitCount++;
    }

    public void printUnits() {
        for (int i = 0; i < unitCount; i++) {
            System.out.println(this.units[i]);
        }
    }

    public boolean isOn(int x, int y) {
        return isInWidth(x) && isHeight(y);
    }

    public boolean isInWidth(int x) {
        return x > 0 && x < width;
    }

    public boolean isHeight(int y) {
        return y > 0 && y < height;
    }

    public boolean hasDominate(int x, int y) {
        for (int i = 0; i < unitCount; i++) {
            Unit unit = this.units[i];
            if (unit.isOn(x, y))
                ;
            if (unit.isDominate()) {
                return true;
            }
        }
        return false;
    }
}
